// @flow

// React
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// React-Redux
import { connect, Provider } from 'react-redux';

// Redux
import { createStore, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';
// import promise from 'redux-promise';
// import createLogger from 'redux-logger';

// socket.io
import io from 'socket.io-client';

// socket handler
const socket = io('http://localhost:8000');



/**
 * Utilities
 */


const valuekey = (track, step) => {
  return 'value-'+track+'-'+step;
};

const updateSequenceValue = (state, update) => {
  const { track, step, value } = update;
  state[valuekey(track, step)] = value;
  return state;
};






/**
 * Redux store and reducers
 */


const propertyReducer = (state, property) => {
  if (property.class === 'tra') {
    if (property.name === 'bpm') {
      state.bpm = property.value;
    }
    if (property.name === 'div') {
      state.div = property.value;
    }
    if (property.name === 'swing') {
      state.swing = property.value;
    }
  }
  if (property.class === 'seq') {
    if (property.name === 'sequences') {
      state = {
        ...state
      }
      for (var track = 0; track < property.value.length; ++track) {
        for (var step = 0; step < property.value[track].length; ++step) {
          updateSequenceValue(state, { track, step, value: property.value[track][step] });
        }
      }
    }
    if (property.name === 'sequenceValue') {
      state = {
        ...state,
        ...updateSequenceValue({}, property.value)
      }
    }
  }
  return state;
};


function reducer(state = {}, action) {
  switch (action.type) {
    case 'step':
      return {
        ...state,
        index: action.index,
        pattern: action.pattern,
        gates: action.gates
      };
      default:
        return state;
    case 'stateset':
      return {
        ...state,
        ...action.data
      }
    case 'propertyset':
      return {
        ...state,
        ...propertyReducer(state, action.data)
      }
  }
}

// const logger = createLogger();
const middleware = applyMiddleware(
  // Custom socket IO middleware,
  ({ dispatch }) => {
    // ensure that the sequencer client in the web server is running
    socket.on('connect', () => {
      socket.emit('startsequence');
    });

    socket.on('step', (data) => {
      console.log('socket: step', data);
      dispatch({
        type: 'step',
        index: data.i,
        pattern: data.p,
        gates: data.g
      });
    });

    socket.on('stateset', (data) => {
      console.log('socket: stateset', data);
      dispatch({
        type: 'stateset',
        data: data.reduce(propertyReducer, {})
      });
    });

    socket.on('propertyset', (data) => {
      console.log('socket: propertyset', data);
      dispatch({
        type: 'propertyset',
        data
      });
    });

    return next => action => next(action);
  },
  // thunk, promise, logger
);
const store = createStore(
  reducer,
  middleware
);







import './style.scss';

/**
 * Components
 */


function valueToKnobProps(value) {
  return {
    value: (isNaN(value) ? 0 : value),
    muted: value === 'M',
    hold: value === 'H'
  }
}

class _Knob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      muted: false,
      hold: false,
      ...props
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  onChange(event) {
    const { track, step } = this.props;
    let { value } = event.target;
    value = parseInt(value, 10);
    if (!isNaN(value)) {
      socket.emit('propertyset', {
        'class': 'seq',
        'name': 'sequenceValue',
        'value': {
          'track': track,
          'step': step,
          'value': value
        }
      });
      this.setState(valueToKnobProps(value));
    }
  }

  render() {
    const { value, muted, hold } = this.state;
    const disabled = muted || hold;
    return <div className={"knob " + (disabled ? "disabled" : "")}>
      <input value={value} onChange={this.onChange} type="number" />
    </div>;
  }
}
const Knob = connect(
  (state, ownProps) => {
    const stateval = state[valuekey(ownProps.track, ownProps.step)];
    return valueToKnobProps(stateval);
  }
)(_Knob);


class _ModeSwitch extends Component {
  constructor() {
    super();
    this.onClick_M = this.handle_M.bind(this);
    this.onClick_N = this.handle_N.bind(this);
    this.onClick_H = this.handle_H.bind(this);
  }

  handle_M() {
    socket.emit('propertyset', {
      'class': 'seq',
      'name': 'sequenceValue',
      'value': {
        'track': this.props.track,
        'step': this.props.step,
        'value': 'M'
      }
    });
  }
  handle_N() {
    socket.emit('propertyset', {
      'class': 'seq',
      'name': 'sequenceValue',
      'value': {
        'track': this.props.track,
        'step': this.props.step,
        'value': 0
      }
    });
  }
  handle_H() {
    socket.emit('propertyset', {
      'class': 'seq',
      'name': 'sequenceValue',
      'value': {
        'track': this.props.track,
        'step': this.props.step,
        'value': 'H'
      }
    });
  }

  render() {
    const { value } = this.props;
    return <div className="switch">
      <div className={"slider " + value} />
      <div className="positions">
        <div className="position pos-M" onClick={this.onClick_M}>M</div>
        <div className="position pos-N" onClick={this.onClick_N}>&uarr;</div>
        <div className="position pos-H" onClick={this.onClick_H}>H</div>
      </div>
    </div>;
  }
}
const ModeSwitch = connect(
  (state, ownProps) => ({
    value: state[valuekey(ownProps.track, ownProps.step)]
  })
)(_ModeSwitch);


class _StepControl extends Component {
  render() {
    const { track, step } = this.props;
    return <div className="step-control">
      <Knob track={track} step={step} />
      <ModeSwitch track={track} step={step} />
    </div>;
  }
}
const StepControl = connect()(_StepControl);


class _LED extends Component {
  render() {
    const { on } = this.props;
    return <div className={"led " + (on ? "on" : "")} />
  }
}
const LED = connect()(_LED);


const tracks = [0, 1, 2, 3];
const steps = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf]

class _SequencerStep extends Component {
  render() {
    const { step , current } = this.props;
    const on = step === current;
    return <div className="step">
      <div className={"num " + (on ? "on": "")}>{step}</div>
      {
        tracks.map(
          (v, i) => <StepControl step={step} track={i} key={i} />
        )
      } <br />
      <LED on={on} />
    </div>;
  }
}
const SequencerStep = connect(
  (state) => ({
    current: state.index
  })
)(_SequencerStep);


class _Sequencer extends Component {
  render() {
    return <div className="seq">
      {steps.map(
        step => <SequencerStep
          key={step}
          step={step}
        />
      )}
    </div>;
  }
}
const Sequencer = connect()(_Sequencer);


class _Transport extends Component {
  constructor(props) {
    super(props);
    this.state = props;
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps);
  }

  onChange(event) {
    let { name, value } = event.target;
    value = parseFloat(value);
    if (!isNaN(value)) {
      this.setState({
        [name]: value
      });

      socket.emit('propertyset', {
        'class': 'tra',
        'name': name,
        'value': value
      });
    }
  }

  render() {
    const { bpm, swing, div } = this.state;
    return <div className="tra">
      <div>
        BPM: <input type="number" onChange={this.onChange} name="bpm" value={bpm || 120} />
      </div>
      <div>
        Swing: <input type="number" onChange={this.onChange} name="swing" value={swing || 0.5} step="0.05" />
      </div>
      <div>
        Division: <input type="number" onChange={this.onChange} name="div" value={div || 4} />
      </div>
    </div>;
  }
}
const Transport = connect(
  (state) => ({
    bpm: state.bpm,
    swing: state.swing,
    div: state.div,
  })
)(_Transport);


class _PatternScopeChannelSlot extends Component {
  constructor() {
    super();
    this.state = {
      value: 0,
      gate: 0
    };
  }

  componentWillReceiveProps(nextProps) {
    const { value, gate } = nextProps;
    if (!isNaN(value)) {
      this.setState({
        value
      });
    }
    if (!isNaN(gate)) {
      this.setState({
        gate
      });
    }
  }

  render() {
    const { value, gate } = this.state;
    const style = {
      top: (24 - value) + 'px',
      borderColor: gate ? '#00d' : '#444'
    }
    return <div className="slot" style={style}>
      {value}
    </div>;
  }
}
const PatternScopeChannelSlot = connect(
  (state, ownProps) => {
    const updateSelf = ownProps.step === state.index;
    const dacValue = (state.pattern || [])[ownProps.track];
    const gateValue = (state.gates || [])[ownProps.track];
    return {
      value: updateSelf ? dacValue : undefined,
      gate: updateSelf ? gateValue : undefined
    }
  }
)(_PatternScopeChannelSlot);


class _PatternScope extends Component {
  render() {
    return <div className="pattern-scope">
      {
        tracks.map(
          track => <div className="channel" key={track}>{
              steps.map(
                step => <PatternScopeChannelSlot key={track+'-'+step} step={step} track={track} />
              )
            }</div>
        )
      }
    </div>;
  }
}
const PatternScope = connect()(_PatternScope);


ReactDOM.render(
  <Provider store={store}>
    <div>
      <Transport bpm={120} swing={0.5} div={4} />
      <Sequencer />
      <PatternScope />
    </div>
  </Provider>,
  document.getElementById('root')
);
